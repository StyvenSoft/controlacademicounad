﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ControlAcadémicoUNAD
{
    class Periodo
    {
        public Curso[] datos;
        public int registrados;
        public int cantidad;
        public int totalcredito;

        public Periodo()
        {
            registrados = 0;
            cantidad = 10;
            datos = new Curso[cantidad];
            totalcredito = 21;
           
        }

        public void RegistrarAsignatura()
        {
            string linea;    
            do
            {
                Console.Write("Ingrese la cantida de asignaturas (1 - 10): ");
                linea = Console.ReadLine();
                registrados = int.Parse(linea);

            }while (registrados <1 || registrados >10);
            
            for (int i = 0; i < registrados; i++)
            {
                datos[i] = new Curso();
                datos[i].codigo = 0;
            }
            validarAsignatura();           
         }

        public void validarAsignatura()
        {
            bool encontrado = false;
            for (int i = 0; i < registrados; i++)
            {
                Curso curso = new Curso();
                encontrado = false;
                curso.RegistrarCodigo();

                for (int j = 0; j < registrados; j++)
                {
                    if (curso.codigo == datos[j].codigo)
                    {
                        encontrado = true;
                        i--;
                        break;
                    }
                }

                if (encontrado == true)
                {
                    Console.WriteLine("El curso ya esta registrado!");
                }

                else
                {
                    if (totalcredito < 1)
                    {
                        Console.WriteLine("Error exceso creditos");
                        break;
                    }
                    else
                    {
                        Console.WriteLine("\n(Creditos disponibles: " + totalcredito+ ") ");
                        curso.RegistrarCurso();
                        curso.ValidarActividad();
                        curso.PuntosFaltantes();
                        curso.CalcularPorcentaje();
                        datos[i].codigo = curso.codigo;
                        datos[i].nombre = curso.nombre;
                        datos[i].credito = curso.credito;
                        datos[i].puntosActividad1 = curso.puntosActividad1;
                        datos[i].puntosActividad2 = curso.puntosActividad2;
                        datos[i].puntosActividad3 = curso.puntosActividad3;
                        datos[i].pfaltantes = curso.pfaltantes;
                        datos[i].porcentaje = curso.porcentaje;
                        datos[i].totalnota = curso.totalnota;
                        totalcredito = totalcredito - datos[i].credito;
                        Console.WriteLine("Correctamente!");
                    }

                }
            }
        }

        public void imprimirNotas()
        {
            Console.WriteLine("\n\tNOTAS DE PERIODO ACADEMICO");

                for (int i = 0; i < registrados; i++)
                {
                    Console.WriteLine("\nNombre: " + datos[i].nombre);
                    Console.WriteLine("Codigo: " + datos[i].codigo); 
                    Console.WriteLine("Numero de creditos: " + datos[i].credito);
                    Console.WriteLine("1. Fases y etapas del curso: " + datos[i].puntosActividad1);
                    Console.WriteLine("2. Sesion de B-Learning y otras actividades: " + datos[i].puntosActividad2);
                    Console.WriteLine("3. Componente practico: " + datos[i].puntosActividad3);
                    Console.WriteLine("Puntos faltantes de la asignatura: " + datos[i].pfaltantes);
                    Console.WriteLine("Porcentaje aprobado: " + datos[i].porcentaje + "%");
                    Console.WriteLine("Total nota: " + datos[i].totalnota);
                }
            Console.WriteLine("Pulse una tecla para continuar..");
            Console.ReadKey();           
        }   
        public void generarAprobados()
        {
            Console.WriteLine("\n\tLISTA DE ASIGNATURAS APROBADAS");
            for (int i = 0; i < registrados; i++)
            {
                if (datos[i].totalnota >= 300)
                {
                    Console.WriteLine("\nNombre: " + datos[i].nombre);
                    Console.WriteLine("Codigo: " + datos[i].codigo);
                    Console.WriteLine("Puntos faltantes de la asignatura: " + datos[i].pfaltantes);
                    Console.WriteLine("Porcentaje aprobado: " + datos[i].porcentaje + "%");
                    Console.WriteLine("Total nota: " + datos[i].totalnota);
                }
            }
            Console.WriteLine("Pulse una tecla para continuar..");
            Console.ReadKey();
        }  

        public void menu()
        {         
            int opcion = 0;
            do
            {
                Console.WriteLine("\n\tCONTROL ACADEMICO UNAD");
                Console.WriteLine("\n1. Generar lista de cursos aprobados");
                Console.WriteLine("2. Consultar notas obtenidas");
                Console.WriteLine("3. salir");
                Console.Write("Elige una opcion: ");
                opcion = Convert.ToByte(Console.ReadLine());
                if (opcion == 1)
                {
                    generarAprobados();
                }
                else if(opcion == 2)
                {
                    imprimirNotas();
                }
            } while (opcion != 3);
        }
    }
    
}
