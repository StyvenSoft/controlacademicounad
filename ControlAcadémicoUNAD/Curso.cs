﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ControlAcadémicoUNAD
{
   
    class Curso
    {
        public string nombre;
        public int codigo;
        public int credito;
        public int actividad;
        public int puntaje;
        public int puntosActividad1;
        public int puntosActividad2;
        public int puntosActividad3;
        public int totalnota;
        public float porcentaje;
        public int pfaltantes;

        public Curso()
        {
            codigo = 0;
            nombre = "";
            credito = 0;
            actividad = 0;
            puntaje = 0;
            porcentaje = 0;
            pfaltantes = 0;
            totalnota = 0;           
        }

        public void RegistrarCodigo()
        {
            string linea;
            Console.Write("\nIngrese codigo del curso: ");
            linea = Console.ReadLine();
            codigo = int.Parse(linea);

        }
        
        public void RegistrarCurso()
        {
            Console.Write("\nIngrese cantidad de creditos: ");
            credito = int.Parse(Console.ReadLine());

            while (credito < 1 || credito > 21)
            {
                Console.WriteLine("Creditos incorrecto!");
                Console.Write("Ingrese nuevamente la cantida de creditos: ");
                credito = int.Parse(Console.ReadLine());
            }

            Console.Write("\nIngrese nombre del curso: ");
            nombre = Console.ReadLine();
            Console.Clear();
        }
        //Metodo para validar la actividad escogida 
        public void ValidarActividad()
        {
            Console.WriteLine("\tCONTROL DE NOTAS UNAD");
            Console.Write("\nCurso: " + nombre);
            int i;
            do
            {
                i = 0;
                if (totalnota >= 500)
                {
                    Console.WriteLine("\nPuntaje maximo obtenido!");
                    i++;
                }
                else
                {
                    string linea;
                    Console.WriteLine("\n 1. Fase o etapa del curso");
                    Console.WriteLine(" 2. Sesion de B-Learning y otras actividades");
                    Console.WriteLine(" 3. Componente practico");
                    Console.WriteLine(" 4. Terminar registro");
                    Console.Write("Elige una opcion: ");
                    linea = Console.ReadLine();
                    actividad = int.Parse(linea);
                    if (actividad > 0 && actividad < 4)
                    {
                        Console.WriteLine("Registrar nota");
                        ValidarNota();
                        Console.WriteLine("\n Se ha registrado correctamente!");
                    }
                    else
                    {
                        Console.WriteLine("salir");
                        i++;
                    }
                }

            } while (i != 1);
        }
        //Metodo para validar la nota ingresada segun la actividad
        public void ValidarNota()
        {
            if (actividad == 1 || actividad == 3)
            {
                if (actividad == 1)
                {
                    Console.WriteLine("\n   Fase y etapa del curso");
                }
                else
                {
                    Console.WriteLine("\n   Registrar nota del componente Practico: ");
                }
                string linea;
                Console.Write("   Ingrese puntaje: ");

                linea = Console.ReadLine();
                puntaje = int.Parse(linea);
                while (puntaje < 0 || puntaje > 125)
                {
                    Console.WriteLine("   ...Puntaje incorrecto!");
                    Console.WriteLine("   Debe digitar un puntaje de entre 0 y 125");
                    Console.Write("   Ingrese nuevamente: ");
                    linea = Console.ReadLine();
                    puntaje = int.Parse(linea);
                }
                CalcularNota(actividad, puntaje);
            }
            else
            {
                string linea;
                Console.Write("\n   Sesion de B-Learning y otras actividades");
                Console.Write("\n   Ingrese puntaje: ");

                linea = Console.ReadLine();
                puntaje = int.Parse(linea);
                while (puntaje < 0 || puntaje > 30)
                {
                    Console.WriteLine("   ...Puntaje incorrecto!");
                    Console.WriteLine("   Debe digitar un puntaje de entre 0 y 30");
                    Console.Write("   Ingrese nuevamente: ");
                    linea = Console.ReadLine();
                    puntaje = int.Parse(linea);
                }
                CalcularNota(actividad, puntaje);
            }

        }
        //Metodo para calcular la nota segun la ctividad seleccionada
        public void CalcularNota(int act, int pj)
        {       
            if (act == 1)
            {
                puntosActividad1 = puntosActividad1 + pj;
            }
            else if (act == 2)
            {
                puntosActividad2 = puntosActividad2 + pj;
            }
            else
            {
                puntosActividad3 = puntosActividad3 + pj;
            }
            totalnota = puntosActividad1 + puntosActividad2 + puntosActividad3;
        }
        //Metodo para calcular el porcentaje total del curso
        public float CalcularPorcentaje()
        {
            if (totalnota < 500)
            {
                porcentaje = totalnota * 100 / 500;
            }
            else
            {
                porcentaje = 100;
            }
            return porcentaje;

        }
        //Metodo para calcular los puntos fantantes del curso
        public void PuntosFaltantes()
        {
            if (totalnota <= 500)
            {
                pfaltantes = 500 - totalnota;
            }
        }
    }
}
